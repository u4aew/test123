$(function(){
    if(!$$.isMobile){
        var $items = $(".socials__item");
        $items.each(function(key, item){
            var $item = $(item),
                timer
                ;

            $item
                .mouseenter(function () {
                    timer = setTimeout(function(){
                        $item.find(".socials-tooltip").fadeIn();
                    }, 200);
                })
                .mouseleave(function () {
                    clearTimeout(timer);
                    $(this).find(".socials-tooltip").fadeOut();
                });

        })

    }

});
