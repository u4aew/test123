$(function(){

    $('[js-toggler]').each(function () {

        var $container = $(this);

        $container.find('[js-toggle]').on('click', function (e) {
            e.preventDefault();
            var $elem = $(this);

            if($elem.hasClass('footer__nav-title_current')){
                $container.find('[js-toggle-content][data-toggle='+$elem.data('toggle')+']').slideUp();
                $elem.removeClass( "footer__nav-title_current" );
            }else{
                $container.find('[js-toggle-content][data-toggle='+$elem.data('toggle')+']').slideDown();
                $elem.addClass( "footer__nav-title_current" );
            }
        });
    });

});