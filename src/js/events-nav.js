$(function(){

    $('[js-opener]').each(function () {

        var $container = $(this);

        $container.find('[js-open]').on('click', function (e) {
            e.preventDefault();
            var $elem = $(this);

            if($elem.hasClass('events-nav__name_current')){
                $container.find('[js-open-content][data-open='+$elem.data('open')+']').slideUp();
                $elem.removeClass( "events-nav__name_current" );
            }else{
                $container.find('[js-open-content][data-open='+$elem.data('open')+']').slideDown();
                $elem.addClass( "events-nav__name_current" );
            }
        });
    });

});