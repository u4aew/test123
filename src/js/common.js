$(function(){

    if(window.ontouchstart || navigator.maxTouchPoints || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)){
        $(':root').attr('touch', true);
    }

    var $registrationBlocks = $('.intro__item_registration, .info__block_registration');

    $('[js-registration-info]').on('click', function(e){
        e.preventDefault();
        $registrationBlocks.eq(1).hide();
        $registrationBlocks.eq(0).show();
    });

    $('[js-registration-back]').on('click', function(e){
        e.preventDefault();
        $registrationBlocks.eq(0).hide();
        $registrationBlocks.eq(1).show();


    });

    $('[data-masked="phone"]').inputmask("+7(999)-999-9999", {showMaskOnHover: false});


    $('[js-tariff-detail]').on('click', function(e){
        e.preventDefault();
        $$.openModal('tariff-detail')
    });

    $('[js-input]').inputShadow();


});


