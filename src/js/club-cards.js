$(function () {
    CheckMasonry()
    $(window).resize(function () {
            CheckMasonry()
        }
    );
    function CheckMasonry() {
        var WindowWidth = $(window).width();
        if (WindowWidth >= 1000) {
            $('.cards__wrapper_club').masonry({
                itemSelector: '.club-card',
                gutter: 25
            });
        }
        else {
            $('.cards__wrapper_club').masonry('destroy');
        }
    }
});